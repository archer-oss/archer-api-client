# Archer API Client

RSA's Archer API Client is an easy-to-use library to enable an application to communicate with the RSA Archer API.

# Discoverable

Many API methods are supported through discoverable and natural method calls. Other APIs can be called using the simple, fluent request builder.

# Simple

The basics of calling the RSA Archer API are handled in the client, making it faster and easier to write the code to work with RSA Archer data.

# Responsible Disclosure of Vulnerabilities
Please follow the directions provided at this link for raising security issues with this library: https://community.rsa.com/docs/DOC-96729. Please do NOT log security issues as GitLab Issues in this repository.
