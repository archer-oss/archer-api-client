﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client.Tests
{
    internal static class SessionContextSource
    {
        public static SessionContext Random()
        {
            return new SessionContext
            {
                InstanceName = DateTime.Now.Ticks.ToString(),
                SessionToken = Guid.NewGuid().ToString(),
                UserId = RandomSource.Random.Next(1000, 5000)
            };
        }
    }

    internal static class ArcherRequestSource
    {
        public static ArcherRequest Random()
        {
            return new ArcherRequest($"api/test/{DateTime.Now.Ticks}");
        }
    }
}