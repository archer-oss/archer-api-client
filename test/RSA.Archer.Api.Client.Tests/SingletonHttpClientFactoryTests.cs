﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSA.Archer.Api.Client.HttpClientFactory;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class SingletonHttpClientFactoryTests
    {
        [TestMethod]
        public void GetHttpClient_Returns_Singleton()
        {
            SingletonHttpClientFactory factory = new SingletonHttpClientFactory();
            var x = factory.GetHttpClient();
            var y = factory.GetHttpClient();
            Assert.AreSame(x, y);
        }

        [TestMethod]
        public void Multiple_Instances_Of_Factory_Do_Not_Share_Singleton()
        {
            SingletonHttpClientFactory factory1 = new SingletonHttpClientFactory();
            SingletonHttpClientFactory factory2 = new SingletonHttpClientFactory();
            var x = factory1.GetHttpClient();
            var y = factory2.GetHttpClient();
            Assert.AreNotSame(x, y);
        }
    }
}
