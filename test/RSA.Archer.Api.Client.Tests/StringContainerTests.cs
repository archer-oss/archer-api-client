﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class StringContainerTests
    {
        [TestMethod]
        public void Value_Property_Sets_Value()
        {
            string g = Guid.NewGuid().ToString();
            StringContainer s = new StringContainer();
            s.Value = g;
            Assert.AreEqual(g, s.Value);
        }

        [TestMethod]
        public void Constructor_Param_Sets_Value_Property()
        {
            string g = Guid.NewGuid().ToString();
            StringContainer s = new StringContainer(g);
            Assert.AreEqual(g, s.Value);
        }
    }
}
