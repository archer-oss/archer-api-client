﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class ArcherRequestExtensionsTests
    {
        [TestMethod]
        public void AddHeader_Handles_No_Existing_Headers()
        {
            string header = "test";
            string value = Guid.NewGuid().ToString();
            var req = ArcherRequestSource.Random();
            req.AddHeader(header, value);
            Assert.IsTrue(req.Headers.ContainsKey(header));
            Assert.AreEqual(req.Headers[header], value);
        }

        [TestMethod]
        public void AddHeader_Retains_Existing_Headers()
        {
            var req = ArcherRequestSource.Random();

            string header = "test";
            string value = Guid.NewGuid().ToString();
            req.AddHeader(header, value);

            string header2 = "test2";
            string value2 = Guid.NewGuid().ToString();
            req.AddHeader(header2, value2);

            Assert.IsTrue(req.Headers.ContainsKey(header));
            Assert.AreEqual(req.Headers[header], value);

            Assert.IsTrue(req.Headers.ContainsKey(header2));
            Assert.AreEqual(req.Headers[header2], value2);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void AddHeader_Throws_ArgumentNullException_On_Null_Header_Name()
        {
            var req = ArcherRequestSource.Random();
            req.AddHeader(null, "test");
        }

        [TestMethod]
        public void AddHeader_Allows_Null_Header_Value()
        {
            var req = ArcherRequestSource.Random();
            req.AddHeader("test", null);
        }

        [TestMethod]
        public void WithHeaders_Handles_No_Existing_Headers()
        {
            int headersCount = RandomSource.Random.Next(1, 4);
            var req = ArcherRequestSource.Random();
            var headers = this.BuildRandomHeaders(headersCount);
            req = req.WithHeaders(headers);
            Assert.AreEqual(headersCount, req.Headers.Count);

            var header = headers.First();
            Assert.IsTrue(req.Headers.ContainsKey(header.Key));
            Assert.AreEqual(headers[header.Key], req.Headers[header.Key]);
        }

        [TestMethod]
        public void WithHeaders_Does_Not_Overwrite_Existing_Headers()
        {
            int headersCount = RandomSource.Random.Next(1, 4);
            var baseReq = ArcherRequestSource.Random();
            string key = Guid.NewGuid().ToString();
            baseReq.AddHeader(key, Guid.NewGuid().ToString());
            var headers = this.BuildRandomHeaders(headersCount);
            var req = baseReq.WithHeaders(headers);

            Assert.IsTrue(req.Headers.ContainsKey(key));
            Assert.AreEqual(baseReq.Headers[key], req.Headers[key]);

            Assert.AreNotEqual(headersCount, req.Headers.Count);

            var header = headers.First();
            Assert.IsTrue(req.Headers.ContainsKey(header.Key));
            Assert.AreEqual(baseReq.Headers[header.Key], req.Headers[header.Key]);
        }

        [TestMethod]
        public void WithHeaders_Accepts_Null()
        {
            ArcherRequestSource.Random().WithHeaders(null);
        }

        [TestMethod]
        public void WithHeaders_Copies_Value_From_Source()
        {
            var value = DateTime.Now;
            var req = ArcherRequestSource.Random().WithValue(value);
            Assert.AreEqual(value, req.Value);
        }

        [TestMethod]
        public void WithHeaders_Copies_ApiPath_From_Source()
        {
            var value = DateTime.Now;
            var baseReq = ArcherRequestSource.Random();
            var req = baseReq.WithValue(value);
            Assert.AreEqual(baseReq.ApiPath, req.ApiPath);
        }

        [TestMethod]
        public void WithHeaders_Copies_SessionContent_From_Source()
        {
            var value = DateTime.Now;
            var baseReq = ArcherRequestSource.Random();
            baseReq.SessionContext = new SessionContext { SessionToken = Guid.NewGuid().ToString() };
            var req = baseReq.WithValue(value);
            Assert.AreEqual(baseReq.SessionContext, req.SessionContext);
        }

        [TestMethod]
        public void WithHeaders_Copies_RequestPreprocessor_From_Source()
        {
            var value = DateTime.Now;
            var baseReq = ArcherRequestSource.Random();
            baseReq.RequestPreprocessor = h => System.Threading.Tasks.Task.CompletedTask;
            var req = baseReq.WithValue(value);
            Assert.AreSame(baseReq.RequestPreprocessor, req.RequestPreprocessor);
        }

        [TestMethod]
        public void WithHeaders_Copies_Headers_From_Source()
        {
            var value = DateTime.Now;
            var baseReq = ArcherRequestSource.Random();
            string key = Guid.NewGuid().ToString();
            baseReq.AddHeader(key, Guid.NewGuid().ToString());
            var req = baseReq.WithValue(value);
            Assert.IsTrue(req.Headers.ContainsKey(key));
            Assert.AreEqual(baseReq.Headers[key], req.Headers[key]);
        }

        [TestMethod]
        public void WithSession_Overwrites_Existing_Session()
        {
            var session1 = SessionContextSource.Random();
            var session2 = SessionContextSource.Random();
            var req = ArcherRequestSource.Random();
            req.WithSession(session1);
            Assert.AreSame(session1, req.SessionContext);
            req.WithSession(session2);
            Assert.AreSame(session2, req.SessionContext);
        }

        [TestMethod]
        public void WithSession_Accepts_Null()
        {
            var req = ArcherRequestSource.Random();
            req.WithSession(SessionContextSource.Random());
            Assert.IsNotNull(req.SessionContext);
            req.WithSession(null);
            Assert.IsNull(req.SessionContext);
        }

        [TestMethod]
        public void WithValue_Overwrites_Existing_Value()
        {
            var stub = TestStub.Random();
            var req = ArcherRequestSource.Random().WithValue(stub);
            Assert.AreSame(stub, req.Value);
            var stub2 = TestStub.Random();
            req = req.WithValue(stub2);
            Assert.AreSame(stub2, req.Value);
        }

        [TestMethod]
        public void WithValue_Accepts_Null()
        {
            var req = ArcherRequestSource.Random().WithValue(TestStub.Random());
            Assert.IsNotNull(req.Value);
            req = req.WithValue((TestStub)null);
            Assert.IsNull(req.Value);
        }

        private Dictionary<string, string> BuildRandomHeaders(int count)
        {
            return Enumerable.Range(1, count).ToDictionary(n => string.Concat("test", n), n => Guid.NewGuid().ToString());
        }
    }
}
