﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSA.Archer.Api.Client.HttpClientFactory;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class TransientHttpClientFactoryTests
    {
        [TestMethod]
        public void GetHttpClient_Returns_New_Object_Each_Time()
        {
            TransientHttpClientFactory source = new TransientHttpClientFactory();
            var x = source.GetHttpClient();
            var y = source.GetHttpClient();
            Assert.AreNotSame(x, y);
        }
    }
}
