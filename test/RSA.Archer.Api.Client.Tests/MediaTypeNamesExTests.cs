﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Net.Mime;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class MediaTypeNamesExTests
    {
        [TestMethod]
        public void Application_Json_Contains_Expected_Value()
        {
            Assert.AreEqual("application/json", MediaTypeNamesEx.Application.Json);
        }
    }
}