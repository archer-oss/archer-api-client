﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using RSA.Archer.Api.Client.HttpClientFactory;

namespace RSA.Archer.Api.Client.Tests
{
    internal class TestHttpClientFactory : IHttpClientFactory
    {
        private readonly Lazy<HttpClient> lazy;

        public TestHttpClientFactory()
            : this(string.Empty)
        {
        }

        public TestHttpClientFactory(Expression<Func<HttpRequestMessage, bool>> requestMessageMatch)
            : this(string.Empty, requestMessageMatch)
        {
        }

        public TestHttpClientFactory(string responseContent, Expression<Func<HttpRequestMessage, bool>> requestMessageMatch = null)
            : this(() => CreateHttpMessageHandler(responseContent, requestMessageMatch))
        {
        }

        public TestHttpClientFactory(HttpResponseMessage response, Expression<Func<HttpRequestMessage, bool>> requestMessageMatch = null)
            : this(() => CreateHttpMessageHandler(response, requestMessageMatch))
        {
        }

        public TestHttpClientFactory(Func<HttpMessageHandler> messageHandlerFactory)
        {
            lazy = new Lazy<HttpClient>(() => new HttpClient(messageHandlerFactory()));
        }

        public int FactoryCallCount { get; private set; }

        public HttpClient GetHttpClient()
        {
            FactoryCallCount++;
            return lazy.Value;
        }

        private static HttpMessageHandler CreateHttpMessageHandler(string responseContent, Expression<Func<HttpRequestMessage, bool>> requestMessageMatch = null)
        {
            HttpResponseMessage response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(responseContent)
            };
            return CreateHttpMessageHandler(response, requestMessageMatch);
        }

        private static HttpMessageHandler CreateHttpMessageHandler(HttpResponseMessage response, Expression<Func<HttpRequestMessage, bool>> requestMessageMatch = null)
        {
            object requestMessageArg = requestMessageMatch != null
                ? ItExpr.Is(requestMessageMatch)
                : ItExpr.IsAny<HttpRequestMessage>();

            Mock<HttpMessageHandler> mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", requestMessageArg, ItExpr.IsAny<CancellationToken>())
                .Returns(Task.FromResult(response));

            return mockHandler.Object;
        }
    }
}