﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client.Tests
{
    internal class TestStub
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Timestamp { get; set; }
        public bool Flag { get; set; }

        #endregion

        #region Static

        public static TestStub Random()
        {
            return new TestStub
            {
                Id = RandomSource.Random.Next(1000, 9000),
                Name = Guid.NewGuid().ToString(),
                Timestamp = DateTime.Now.AddDays(RandomSource.Random.Next(1000)),
                Flag = true
            };
        }

        #endregion
    }
}
