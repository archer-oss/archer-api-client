﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net.Http;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class ArcherResponseExtensionsTests
    {
        [TestMethod, ExpectedException(typeof(HttpRequestException))]
        public void EnsureSuccessful_Throws_Exception_If_Single_Response_Not_Successful()
        {
            var response = new ArcherResponse { IsSuccessful = false };
            response.EnsureSuccessful();
        }

        [TestMethod]
        public void EnsureSuccessful_Does_Not_Throw_Exception_If_Single_Response_Successful()
        {
            var response = new ArcherResponse { IsSuccessful = true };
            response.EnsureSuccessful();
        }

        [TestMethod, ExpectedException(typeof(HttpRequestException))]
        public void EnsureSuccessful_Throws_Exception_If_Multiple_Responses_Not_Successful()
        {
            var responses = new List<ArcherResponse>
            {
                new ArcherResponse {IsSuccessful = false},
                new ArcherResponse {IsSuccessful = true},
                new ArcherResponse {IsSuccessful = false},
                new ArcherResponse {IsSuccessful = true},
            };
            responses.EnsureSuccessful();
        }

        [TestMethod]
        public void EnsureSuccessful_Exception_Contains_ValidationMessage_ResourcedMessage()
        {
            string message = string.Concat("Error: ", Guid.NewGuid());

            try
            {
                var response = new ArcherResponse
                {
                    IsSuccessful = false,
                    ValidationMessages = new List<ValidationMessage>
                    {
                        new ValidationMessage {ResourcedMessage = message, Description = "unused"}
                    }
                };
                response.EnsureSuccessful();
            }
            catch (HttpRequestException e)
            {
                Assert.AreEqual(message, e.Message);
            }
        }

        [TestMethod]
        public void EnsureSuccessful_Exception_Contains_ValidationMessage_Description_When_ResourcedMessage_Empty()
        {
            string message = string.Concat("Error: ", Guid.NewGuid());
            try
            {
                var response = new ArcherResponse
                {
                    IsSuccessful = false,
                    ValidationMessages = new List<ValidationMessage>
                    {
                        new ValidationMessage {Description = message}
                    }
                };
                response.EnsureSuccessful();
            }
            catch (HttpRequestException e)
            {
                Assert.AreEqual(message, e.Message);
            }
        }
    }
}