﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class HeadersTests
    {
        [TestMethod]
        public void X_HTTP_METHOD_OVERRIDE_Contains_Expected_Value()
        {
            Assert.AreEqual("X-Http-Method-Override", Headers.X_HTTP_METHOD_OVERRIDE);
        }
    }
}