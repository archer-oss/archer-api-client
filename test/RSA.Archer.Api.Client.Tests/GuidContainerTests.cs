﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class GuidContainerTests
    {
        [TestMethod]
        public void Value_Property_Sets_Value()
        {
            Guid g = Guid.NewGuid();
            GuidContainer s = new GuidContainer();
            s.Value = g;
            Assert.AreEqual(g, s.Value);
        }

        [TestMethod]
        public void Constructor_Param_Sets_Value_Property()
        {
            Guid g = Guid.NewGuid();
            GuidContainer s = new GuidContainer(g);
            Assert.AreEqual(g, s.Value);
        }
    }
}