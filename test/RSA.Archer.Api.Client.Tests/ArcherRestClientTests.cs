﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using RSA.Archer.Api.Client.Authentication;

namespace RSA.Archer.Api.Client.Tests
{
    [TestClass]
    public class ArcherRestClientTests
    {
        private const string HOST = "http://testhost/archer/api";

        [DataRow("http://testhost/app/api/", "/test/endpoint/")]
        [DataRow("http://testhost/app/api/", "test/endpoint/")]
        [DataRow("http://testhost/app/api", "/test/endpoint/")]
        [DataRow("http://testhost/app/api", "test/endpoint/")]
        [DataTestMethod]
        public void BuildRequestUri_Combines_Host_And_Path(string host, string apiPath)
        {
            string expected = $"{host.TrimEnd('/')}/{apiPath.TrimStart('/')}";
            ArcherRestClient client = new ArcherRestClient(host);
            var uri = client.BuildRequestUri(apiPath);
            string actual = uri.AbsoluteUri;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BuildRequestUri_Trims_Starting_Slash_From_Path()
        {
            string apiPath = $"/test/path/{DateTime.Now.Ticks}/";
            string expected = $"{HOST}/{apiPath.Substring(1)}";
            ArcherRestClient client = new ArcherRestClient(HOST);
            var uri = client.BuildRequestUri(apiPath);
            string actual = uri.AbsoluteUri;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void BuildRequestUri_Handles_Null_ApiPath_Parameter()
        {
            string apiPath = null;
            string expected = $"{HOST}/";
            ArcherRestClient client = new ArcherRestClient(HOST);
            var uri = client.BuildRequestUri(apiPath);
            string actual = uri.AbsoluteUri;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Constructor_Adds_Missing_Trailing_Slash()
        {
            string host = HOST.TrimEnd('/');
            ArcherRestClient client = new ArcherRestClient(host);
            Assert.IsTrue(client.BaseUri.AbsoluteUri.EndsWith("/"));
        }

        [TestMethod]
        public void Constructor_Does_Not_Add_Trailing_Slash_If_Exists()
        {
            string host = HOST;
            if (!host.EndsWith("/"))
                host += "/";

            ArcherRestClient client = new ArcherRestClient(host);
            Assert.IsTrue(client.BaseUri.AbsoluteUri.EndsWith("/"));
            Assert.IsFalse(client.BaseUri.AbsoluteUri.EndsWith("//"));
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_Validates_BaseUri_String_Not_Null()
        {
            ArcherRestClient client = new ArcherRestClient((string)null);
        }

        [TestMethod, ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_Validates_BaseUri_Uri_Not_Null()
        {
            ArcherRestClient client = new ArcherRestClient((Uri)null);
        }

        [TestMethod]
        public async Task Response_Deserialized_Into_Object()
        {
            TestStub stub = TestStub.Random();
            string responseJson = JsonConvert.SerializeObject(stub);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(responseJson));
            var resp = await client.SendAsync<TestStub>(HttpMethod.Get, ArcherRequestSource.Random());
            Assert.AreEqual(stub.Id, resp.Id);
            Assert.AreEqual(stub.Name, resp.Name);
            Assert.AreEqual(stub.Timestamp, resp.Timestamp);
            Assert.AreEqual(stub.Flag, resp.Flag);
        }

        [TestMethod]
        public async Task Post_Uses_Correct_Http_Method()
        {
            TestStub stub = TestStub.Random();
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Method == HttpMethod.Post));
            await client.PostAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Post_Content_Has_Json_MediaType()
        {
            TestStub stub = TestStub.Random();
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Content.Headers.ContentType.MediaType == System.Net.Mime.MediaTypeNamesEx.Application.Json));
            await client.PostAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Post_Passes_Content_To_Request_Message()
        {
            TestStub stub = TestStub.Random();
            string json = JsonConvert.SerializeObject(stub);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => StringContent_Matches_Json((StringContent)x.Content, json)));
            await client.PostAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Post_Passes_ArcherRequest_Value_To_Request_Message()
        {
            TestStub stub = TestStub.Random();
            string json = JsonConvert.SerializeObject(stub);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => StringContent_Matches_Json((StringContent)x.Content, json)));
            await client.PostAsync<TestStub, string>(ArcherRequestSource.Random().WithValue(stub)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Put_Uses_Correct_Http_Method()
        {
            TestStub stub = TestStub.Random();
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Method == HttpMethod.Put));
            await client.PutAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Put_Content_Has_Json_MediaType()
        {
            TestStub stub = TestStub.Random();
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Content.Headers.ContentType.MediaType == System.Net.Mime.MediaTypeNamesEx.Application.Json));
            await client.PutAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Put_Passes_Content_To_RequestMessage()
        {
            TestStub stub = TestStub.Random();
            string json = JsonConvert.SerializeObject(stub);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => StringContent_Matches_Json((StringContent)x.Content, json)));
            await client.PutAsync<TestStub, string>(ArcherRequestSource.Random(), stub).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Put_Passes_ArcherRequest_Value_To_Request_Message()
        {
            TestStub stub = TestStub.Random();
            string json = JsonConvert.SerializeObject(stub);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => StringContent_Matches_Json((StringContent)x.Content, json)));
            await client.PutAsync<TestStub, string>(ArcherRequestSource.Random().WithValue(stub)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Delete_Uses_Correct_Http_Method()
        {
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Method == HttpMethod.Delete));
            await client.DeleteAsync<string>(ArcherRequestSource.Random()).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Get_Uses_Correct_Http_Method()
        {
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Method == HttpMethod.Get));
            await client.GetAsync<string>(ArcherRequestSource.Random()).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task SessionContext_Results_In_Correct_Authorization_Header()
        {
            SessionContext session = SessionContextSource.Random();
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => Authorization_Header_Has_Archer_SessionToken(x.Headers.Authorization, session)));
            await client.GetAsync<string>(ArcherRequestSource.Random().WithSession(session)).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task No_SessionContext_Results_In_No_Authorization_Header()
        {
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Headers.Authorization == null));
            await client.GetAsync<string>(ArcherRequestSource.Random()).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task Headers_Get_Added_To_Request()
        {
            string header = $"Header{RandomSource.Random.Next(20, 59)}";
            string headerValue = DateTime.Now.Ticks.ToString();
            var archerRequest = ArcherRequestSource.Random().AddHeader(header, headerValue);
            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory(x => x.Headers.Single(h => h.Key == header).Value.Contains(headerValue)));
            await client.GetAsync<string>(archerRequest).ConfigureAwait(false);
        }

        [TestMethod]
        public async Task RequestPreprocessor_Called_During_Request()
        {
            bool actionCalled = false;
            var archerRequest = ArcherRequestSource.Random();
            archerRequest.RequestPreprocessor = h =>
            {
                actionCalled = true;
                return Task.CompletedTask;
            };

            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory());
            await client.GetAsync<string>(archerRequest).ConfigureAwait(false);
            Assert.IsTrue(actionCalled);
        }

        [TestMethod]
        public async Task ResponsePreprocessor_Called_During_Request()
        {
            bool actionCalled = false;
            var archerRequest = ArcherRequestSource.Random();
            archerRequest.ResponsePreprocessor = h =>
            {
                actionCalled = true;
                return Task.CompletedTask;
            };

            ArcherRestClient client = new ArcherRestClient(HOST, new TestHttpClientFactory());
            await client.GetAsync<string>(archerRequest).ConfigureAwait(false);
            Assert.IsTrue(actionCalled);
        }

        [TestMethod]
        public async Task N_Requests_Calls_HttpClientFactory_N_Times()
        {
            TestHttpClientFactory factory = new TestHttpClientFactory();
            ArcherRestClient client = new ArcherRestClient(HOST, factory);

            int n = RandomSource.Random.Next(3, 9);
            for (int i = 0; i < n; i++)
                await client.GetAsync<string>(ArcherRequestSource.Random()).ConfigureAwait(false);

            Assert.AreEqual(n, factory.FactoryCallCount);
        }

        private bool Authorization_Header_Has_Archer_SessionToken(AuthenticationHeaderValue headerValue, SessionContext sessionContext)
        {
            ArcherSessionAuthenticationHeaderValue archAuth = headerValue as ArcherSessionAuthenticationHeaderValue;
            return archAuth?.SessionToken == sessionContext.SessionToken;
        }

        private bool StringContent_Matches_Json(StringContent content, string json)
        {
            string contentString = content.ReadAsStringAsync().Result;
            return contentString == json;
        }
    }
}
