﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client.Tests
{
    internal static class RandomSource
    {
        private static readonly Lazy<Random> lazy = new Lazy<Random>();

        public static Random Random => lazy.Value;
    }
}