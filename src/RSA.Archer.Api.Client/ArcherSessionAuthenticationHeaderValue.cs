﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Net.Http.Headers;

namespace RSA.Archer.Api.Client
{
    public class ArcherSessionAuthenticationHeaderValue : AuthenticationHeaderValue
    {
        public ArcherSessionAuthenticationHeaderValue(SessionContext sessionContext) : this(sessionContext?.SessionToken)
        {
        }

        public ArcherSessionAuthenticationHeaderValue(string sessionToken) : base("Archer", "session-id=" + sessionToken)
        {
            SessionToken = sessionToken;
        }

        public string SessionToken { get; }
    }
}
