﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Collections.Generic;

namespace RSA.Archer.Api.Client.Content
{
    public class FieldContentRequest
    {
        public IList<int> ContentIds { get; set; }
        public IList<int> FieldIds { get; set; }
    }
}
