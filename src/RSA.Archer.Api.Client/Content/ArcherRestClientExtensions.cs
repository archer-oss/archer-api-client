﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Content
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<ContentRecord> GetContentAsync(this ArcherRestClient source, int contentId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/content/{contentId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<ContentRecord>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<IList<ContentRecord>> GetContentForReferenceFieldAsync(this ArcherRestClient source, int referenceFieldId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/content/referenceField/{referenceFieldId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<ContentRecord>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<IList<ContentRecord>> GetContentForFieldAsync(this ArcherRestClient source, IList<int> contentIds, IList<int> fieldIds, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            FieldContentRequest reqBody = new FieldContentRequest { ContentIds = contentIds, FieldIds = fieldIds };
            var request = source.BuildRequest("core/content/fieldContent/").WithSession(sessionContext);
            var response = await source.PostAsync<FieldContentRequest, ArcherResponse<ContentRecord>[]>(request, reqBody).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<ArcherResponse<ContentSaveResponse>> PostContentAsync(this ArcherRestClient source, ContentRequest contentRequest, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/content/").WithSession(sessionContext).WithValue(contentRequest);
            var response = await source.PostAsync<ContentRequest, ArcherResponse<ContentSaveResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response;
        }

        public static async Task<ArcherResponse<ContentSaveResponse>> PutContentAsync(this ArcherRestClient source, ContentRequest contentRequest, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/content/").WithSession(sessionContext).WithValue(contentRequest);
            var response = await source.PutAsync<ContentRequest, ArcherResponse<ContentSaveResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response;
        }

        public static async Task<ArcherResponse> DeleteContentAsync(this ArcherRestClient source, int contentId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/content/{contentId}").WithSession(sessionContext);
            var response = await source.DeleteAsync<ArcherResponse>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response;
        }
    }
}
