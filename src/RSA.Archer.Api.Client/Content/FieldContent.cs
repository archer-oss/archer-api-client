﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Dynamic;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content
{
    public class FieldContent
    {
        public int FieldId { get; set; }
        public FieldType Type { get; set; }
        public object Value { get; set; }
    }

    public class FieldContent<T>
    {
        public int FieldId { get; set; }
        public FieldType Type { get; set; }
        public T Value { get; set; }
    }
}
