﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using RSA.Archer.Api.Client.Content.FieldContentConverters;

namespace RSA.Archer.Api.Client.Content
{
    public static class FieldContentExtensions
    {
        private static readonly Lazy<FieldContentConverterFactory> fieldContentConverterFactoryLazy = new Lazy<FieldContentConverterFactory>();

        public static FieldContent<T> ReadAs<T>(this FieldContent source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var converter = fieldContentConverterFactoryLazy.Value.GetConverter<T>(source.Type);
            if (converter == null) throw new InvalidOperationException($"No converter found for field type '{source.Type.ToString()}' with value of type '{typeof(T).Name}'.");
            return converter.Convert(source);
        }
    }
}
