﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Content
{
    public class ContentRecord
    {
        public int? Id { get; set; }
        public int LevelId { get; set; }
        public int? SequentialId { get; set; }
        public DateTime LastUpdated { get; set; }
        public int? Version { get; set; }
        public IDictionary<int, FieldContent> FieldContents { get; set; }
    }
}
