﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json.Linq;
using RSA.Archer.Api.Client.Content.FieldContents;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class CrossReferenceFieldContentConverter : FieldContentConverter<CrossReferenceContent>
    {
        protected override CrossReferenceContent ConvertValue(object value)
        {
            if (value == null) return null;
            var vals = ((JArray)value).Values<JObject>().ToList();
            var referencedContents = vals.Select(ReadJObject).ToList();
            return new CrossReferenceContent { ReferencedContents = referencedContents };
        }

        private ReferencedContent ReadJObject(JObject jObject)
        {
            var props = jObject.Properties().ToList();
            return new ReferencedContent
            {
                ContentId = props.FirstOrDefault(x => x.Name.Equals("ContentId", System.StringComparison.OrdinalIgnoreCase))?.Value?.Value<int>() ?? 0,
                LevelId = props.FirstOrDefault(x => x.Name.Equals("LevelId", System.StringComparison.OrdinalIgnoreCase))?.Value?.Value<int>() ?? 0
            };
        }

        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.CrossReference;
    }
}