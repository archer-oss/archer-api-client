﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class FieldContentConverterFactory : IFieldContentConverterFactory
    {
        private static readonly Lazy<List<IFieldContentConverter>> allConverters = new Lazy<List<IFieldContentConverter>>(DiscoverConverters);

        private static List<IFieldContentConverter> DiscoverConverters()
        {
            return new List<IFieldContentConverter>
            {
                new TextFieldContentConverter(),
                new NumericFieldContentConverter(),
                new DateFieldContentConverter(),
                new TrackingIdFieldContentConverter(),
                new SubFormFieldContentConverter(),
                new CrossReferenceFieldContentConverter(),
                new FirstPublishedDateFieldContentConverter(),
                new LastUpdatedDateFieldContentConverter()
            };
        }

        public IFieldContentConverter<T> GetConverter<T>(FieldType fieldType)
        {
            return allConverters.Value.OfType<IFieldContentConverter<T>>().FirstOrDefault(x => x.CanConvert(fieldType));
        }
    }
}
