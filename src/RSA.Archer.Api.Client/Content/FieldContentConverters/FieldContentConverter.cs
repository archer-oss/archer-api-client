﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public abstract class FieldContentConverter<T> : IFieldContentConverter<T>
    {
        public FieldContent<T> Convert(FieldContent fieldContent)
        {
            if (fieldContent == null) throw new ArgumentNullException(nameof(fieldContent));

            FieldContent<T> result = new FieldContent<T>
            {
                FieldId = fieldContent.FieldId,
                Type = fieldContent.Type,
                Value = ConvertValue(fieldContent.Value)
            };

            return result;
        }

        protected abstract T ConvertValue(object value);
        public abstract bool CanConvert(FieldType fieldType);
    }
}
