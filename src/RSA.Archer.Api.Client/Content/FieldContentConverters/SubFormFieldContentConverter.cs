﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using RSA.Archer.Api.Client.Content.FieldContents;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class SubFormFieldContentConverter : FieldContentConverter<SubFormContent>
    {
        protected override SubFormContent ConvertValue(object value)
        {
            if (value == null) return null;
            List<int> contentIds = ((JArray)value).Values<int>().ToList();
            return new SubFormContent { ContentIds = contentIds };
        }

        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.SubForm;
    }
}