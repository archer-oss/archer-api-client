﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public interface IFieldContentConverter
    {
        bool CanConvert(FieldType fieldType);
    }

    public interface IFieldContentConverter<T> : IFieldContentConverter
    {
        FieldContent<T> Convert(FieldContent fieldContent);
    }
}