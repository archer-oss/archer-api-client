﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Globalization;
using RSA.Archer.Api.Client.Content.FieldContents;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class DateFieldContentConverter : FieldContentConverter<DateContent>
    {
        protected override DateContent ConvertValue(object value) => value == null ? null : new DateContent { Date = System.Convert.ToDateTime(value, CultureInfo.InvariantCulture) };
        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.Date;
    }

    public class FirstPublishedDateFieldContentConverter : FieldContentConverter<FirstPublishedDateContent>
    {
        protected override FirstPublishedDateContent ConvertValue(object value) => value == null ? null : new FirstPublishedDateContent { FirstPublished = System.Convert.ToDateTime(value, CultureInfo.InvariantCulture) };
        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.FirstPublishedDate;
    }

    public class LastUpdatedDateFieldContentConverter : FieldContentConverter<LastUpdatedDateContent>
    {
        protected override LastUpdatedDateContent ConvertValue(object value) => value == null ? null : new LastUpdatedDateContent { LastUpdated = System.Convert.ToDateTime(value, CultureInfo.InvariantCulture) };
        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.LastUpdatedDate;
    }
}
