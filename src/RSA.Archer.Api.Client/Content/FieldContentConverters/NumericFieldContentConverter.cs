﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Globalization;
using Newtonsoft.Json;
using RSA.Archer.Api.Client.Content.FieldContents;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class NumericFieldContentConverter : FieldContentConverter<NumericContent>
    {
        protected override NumericContent ConvertValue(object value) => value == null ? null : new NumericContent { NumericValue = System.Convert.ToDouble(value, CultureInfo.InvariantCulture) };
        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.Numeric;
    }
}