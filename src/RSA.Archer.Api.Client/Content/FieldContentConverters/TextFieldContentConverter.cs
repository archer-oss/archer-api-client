﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Globalization;
using RSA.Archer.Api.Client.Content.FieldContents;
using RSA.Archer.Api.Client.Metadata.Fields;

namespace RSA.Archer.Api.Client.Content.FieldContentConverters
{
    public class TextFieldContentConverter : FieldContentConverter<TextContent>
    {
        protected override TextContent ConvertValue(object value) => value == null ? null : new TextContent { Text = System.Convert.ToString(value, CultureInfo.InvariantCulture) };
        public override bool CanConvert(FieldType fieldType) => fieldType == FieldType.Text;
    }
}