﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.Content.FieldContents
{
    public class TextContent
    {
        public string Text { get; set; }
    }
}