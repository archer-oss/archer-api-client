﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Net.Http;

namespace RSA.Archer.Api.Client.HttpClientFactory
{
    public class SingletonHttpClientFactory : IHttpClientFactory
    {
        private readonly Lazy<HttpClient> singleton = new Lazy<HttpClient>();

        public virtual HttpClient GetHttpClient()
        {
            return singleton.Value;
        }
    }
}