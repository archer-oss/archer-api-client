﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Net.Http;

namespace RSA.Archer.Api.Client.HttpClientFactory
{
    public interface IHttpClientFactory
    {
        HttpClient GetHttpClient();
    }
}