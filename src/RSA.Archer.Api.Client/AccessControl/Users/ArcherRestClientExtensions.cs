﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.AccessControl.Users
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<UserResponse> GetUserAsync(this ArcherRestClient source, int userId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/user/{userId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<UserResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<List<UserResponse>> GetUsersAsync(this ArcherRestClient source, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/user/").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<UserResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }
    }
}
