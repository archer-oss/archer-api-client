﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.AccessControl.Users
{
    public class UserResponse
    {
        public int? Id { get; set; }
        public string UserName { get; set; }
        public UserType Type { get; set; }
        public int? DomainId { get; set; }
        public string DisplayName { get; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public AccountStatusType AccountStatus { get; set; }
        public bool ForcePasswordChange { get; set; }
        public int? LanguageId { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string Locale { get; set; }
        public int SecurityId { get; set; }
        public string TimeZoneId { get; set; }
        public string Title { get; set; }
        public string AdditionalNote { get; set; }
        public string Address { get; set; }
        public string BusinessUnit { get; set; }
        public string Company { get; set; }
        public int? DefaultHomeDashboardId { get; set; }
        public int? DefaultHomeWorkspaceId { get; set; }
        public string Department { get; set; }
    }
}
