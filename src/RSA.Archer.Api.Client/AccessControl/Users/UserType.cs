﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.AccessControl.Users
{
    public enum UserType
    {
        Application = 1,
        Service = 2,
        Sysadmin = 3,
        DataFeedService = 4,
        Migration = 5,
        ACP = 6
    }
}
