﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.AccessControl.Users
{
    public enum AccountStatusType
    {
        Active = 1,
        Inactive = 2,
        Locked = 3,
        Deleted = 99999
    }
}
