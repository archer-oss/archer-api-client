﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.AccessControl.Groups
{
   public class GroupResponse
    {
        public int? Id { get; set; }
        public Guid? Guid { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; }
        public string DistinguishedName { get; set; }
        public int? DomainId { get; set; }
        public string Description { get; set; }
        public int? DefaultHomeDashboardId { get; set; }
        public int? DefaultHomeWorkspaceId { get; set; }
        public bool EveryoneFlag { get; set; }
        public bool LdapFlag { get; set; }
        public bool SystemFlag { get; set; }
    }
}
