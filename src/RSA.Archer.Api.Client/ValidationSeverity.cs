﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client
{
    public enum ValidationSeverity
    {
        None = 0,
        Information = 1,
        Warning = 2,
        Error = 3,
        Critical = 4
    }
}
