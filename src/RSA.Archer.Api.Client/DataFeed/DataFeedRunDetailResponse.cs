﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Collections.Generic;

namespace RSA.Archer.Api.Client.DataFeed
{
    public class DataFeedRunDetailResponse
    {
        public DataFeedHistoryResponse DataFeedHistory { get; set; }

        public List<DataFeedMessageResult> DataFeedMessages { get; set; }
    }
}
