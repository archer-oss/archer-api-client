﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client.DataFeed
{
    public class DataFeedHistoryResponse
    {
        /// <summary>
        /// Gets or sets the Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Data Feed Id for the Data Feed to which this history object belongs
        /// </summary>
        public int DataFeedId { get; set; }

        /// <summary>
        /// Gets or sets the job id that the data feed ran under
        /// </summary>
        /// <value>The job id.</value>        
        public Guid JobId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the feed [as manually started.
        /// </summary>
        /// <value><c>true</c> if the feed was manually started; otherwise, <c>false</c>.</value>
        public bool WasManuallyStarted { get; set; }

        /// <summary>
        /// Gets or sets the date and time that this feed history item began execution
        /// </summary>
        public DateTime? StartTime { get; set; }

        /// <summary>
        /// Gets or sets the date and time that this feed history item finished execution
        /// </summary>
        public DateTime? EndTime { get; set; }

        /// <summary>
        /// Gets or sets the status of this feed history item
        /// </summary>
        public DataFeedHistoryStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the number of RecordsProcessed
        /// </summary>
        public int SourceRecordsProcessed { get; set; }

        /// <summary>
        /// Gets or sets Target records modification numbers
        /// </summary>
        public RecordModificationDetail TargetRecords { get; set; }

        /// <summary>
        /// Gets or sets Child records modification numbers
        /// </summary>
        public RecordModificationDetail ChildRecords { get; set; }

        /// <summary>
        /// Gets or sets Subform records modification numbers
        /// </summary>
        public RecordModificationDetail SubFormRecords { get; set; }
    }
}
