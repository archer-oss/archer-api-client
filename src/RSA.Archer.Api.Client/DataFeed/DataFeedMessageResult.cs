﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.DataFeed
{
    public class DataFeedMessageResult 
    {
        public int DatafeedHistoryId { get; set; }
        public string DatafeedMessage { get; set; }
        public string DatafeedMessageId { get; set; }
        public int Row { get; set; }
        public string LocationMessage { get; set; }
        public string LocationParameters { get; set; }
        public string LocationMessageId { get; set; }
        public string DatafeedMessageParameters { get; set; }
        public ValidationSeverity SeverityLevel { get; set; }
    }
}
