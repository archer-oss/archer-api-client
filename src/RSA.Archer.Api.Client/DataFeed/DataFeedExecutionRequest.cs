﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client.DataFeed
{
    public class DataFeedExecutionRequest
    {
        /// <summary>
        /// Gets or sets the data feed guid .
        /// </summary>
        /// <value>
        /// The Data Feed Guid.
        /// </value>
        public Guid DataFeedGuid { get; set; }

        /// <summary>
        /// Gets or sets whether to run reference data feeds.
        /// </summary>
        /// <value>
        /// A flag indicates whether to run the reference feeds.
        /// </value>
        public bool IsReferenceFeedsIncluded { get; set; }
    }
}
