﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.DataFeed
{
    public enum DataFeedStatusType
    {
        Invalid = 0,
        Valid = 1,
        Deleted = 2,
        None = 3
    }
}