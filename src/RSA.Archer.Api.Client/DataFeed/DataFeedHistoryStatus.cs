﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.DataFeed
{
    public enum DataFeedHistoryStatus
    {
        Running = 1,
        Completed = 2,
        Faulted = 3,
        Warning = 4,
        Terminating = 5,
        Terminated = 6,
        Pending = 7
    }
}
