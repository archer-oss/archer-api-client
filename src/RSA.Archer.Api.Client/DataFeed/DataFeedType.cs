﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.DataFeed
{
    public enum DataFeedType
    {
        StandardServiceDataFeed = 1,
        DataImportDataFeed = 2
    }
}