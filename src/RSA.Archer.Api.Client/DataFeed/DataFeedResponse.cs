﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.DataFeed
{
    public class DataFeedResponse
    {
        public int? Id { get; set; }
        public Guid Guid { get; set; }
        public string Name { get; set; }
        public DataFeedType DataFeedType { get; set; }
        public bool Active { get; set; }
        public Guid? ScheduledGuid { get; set; }
        public Guid? ScheduleParentGuid { get; set; }
        public string ScheduleParentName { get; set; }
        public string Alias { get; set; }
        public DataFeedStatusType Status { get; set; }
        public string LastProcessedValue { get; set; }
        public string ConfigurationXml { get; set; }
        public int SelectedTarget { get; set; }
        public bool EnableHolding { get; set; }
    }
}
