﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.DataFeed
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<List<DataFeedResponse>> GetDataFeedsAsync(this ArcherRestClient source, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/datafeed/").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<DataFeedResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<List<DataFeedHistoryResponse>> GetDataFeedHistoryAsync(this ArcherRestClient source, Guid dataFeedGuid, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/datafeed/history/")
                                .AddHeader(Headers.X_HTTP_METHOD_OVERRIDE, HttpMethod.Get.Method)
                                .WithSession(sessionContext)
                                .WithValue(new GuidContainer(dataFeedGuid));

            var response = await source.PostAsync<GuidContainer, ArcherResponse<DataFeedHistoryResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<DataFeedRunDetailResponse> GetDataFeedHistoryRecentAsync(this ArcherRestClient source, Guid dataFeedGuid, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/datafeed/history/recent/")
                                .AddHeader(Headers.X_HTTP_METHOD_OVERRIDE, HttpMethod.Get.Method)
                                .WithSession(sessionContext)
                                .WithValue(new GuidContainer(dataFeedGuid));

            var response = await source.PostAsync<GuidContainer, ArcherResponse<DataFeedRunDetailResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<ArcherResponse> ExecuteDataFeedAsync(this ArcherRestClient source, DataFeedExecutionRequest executionRequest, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/datafeed/execution").WithSession(sessionContext).WithValue(executionRequest);
            var response = await source.PostAsync<DataFeedExecutionRequest, ArcherResponse>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response;
        }
    }
}
