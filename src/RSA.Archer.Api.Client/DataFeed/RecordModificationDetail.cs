﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.DataFeed
{
    public class RecordModificationDetail
    {
        /// <summary>
        /// Gets or sets the number of records created
        /// </summary>
        public int Created { get; set; }

        /// <summary>
        /// Gets or sets the number of records updated
        /// </summary>
        public int Updated { get; set; }

        /// <summary>
        /// Gets or sets the number of records failed
        /// </summary>
        public int Failed { get; set; }

        /// <summary>
        /// Gets or sets the number of records deleted
        /// </summary>
        public int Deleted { get; set; }
    }
}
