﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client
{
    public class SessionContext
    {
        public string InstanceName { get; set; }
        public string SessionToken { get; set; }
        public int UserId { get; set; }
    }
}
