﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client
{
    public class Link
    {
        public string Href { get; set; }
        public string Rel { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
    }
}
