﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client
{
    public class ValidationMessage
    {
        public string Description { get; set; }
        public string ErroredValue { get; set; }
        public int Location { get; set; }
        public string MessageKey { get; set; }
        public string Reason { get; set; }
        public string ResourcedMessage { get; set; }
        public ValidationSeverity Severity { get; set; }
        public string Validator { get; set; }
        public string XmlData { get; set; }
    }
}
