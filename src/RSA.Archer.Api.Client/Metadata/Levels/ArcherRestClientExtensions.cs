﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Metadata.Levels
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<LevelResponse> GetLevelAsync(this ArcherRestClient source, int levelId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/level/{levelId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<LevelResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<List<LevelResponse>> GetLevelsAsync(this ArcherRestClient source, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/system/level/").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<LevelResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }
    }
}
