﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.Levels
{
    public class LevelResponse
    {
        public int? Id { get; set; }
        public Guid? Guid { get; set; }
        public int ModuleId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }
        public bool AllowFilter { get; set; }
        public bool IsDeleted { get; set; }
    }
}
