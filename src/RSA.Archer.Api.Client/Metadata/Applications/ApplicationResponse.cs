﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using RSA.Archer.Api.Client.Metadata.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.Applications
{
    public class ApplicationResponse : ModuleResponse
    {
        public bool IsAuthoratativeSource { get; set; }
        public bool IsDeprecated { get; set; }
        public bool IsDirectToEditEnabled { get; set; }
        public bool IsNotification { get; set; }
        public bool IsTaskManagementEnabled { get; set; }
        public bool KeepLicensed { get; set; }
        public int LanguageId { get; set; }
    }
}
