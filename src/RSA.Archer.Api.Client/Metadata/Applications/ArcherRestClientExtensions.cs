﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Metadata.Applications
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<ApplicationResponse> GetApplicationAsync(this ArcherRestClient source, int applicationId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/application/{applicationId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<ApplicationResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<List<ApplicationResponse>> GetApplicationsAsync(this ArcherRestClient source, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest("core/system/application/").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<ApplicationResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }
    }
}
