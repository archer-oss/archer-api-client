﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.Fields
{
    public class FieldDefinitionResponse
    {
        public int? Id { get; set; }
        public FieldType Type { get; set; }
        public int LevelId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public Guid? Guid { get; set; }
    }
}
