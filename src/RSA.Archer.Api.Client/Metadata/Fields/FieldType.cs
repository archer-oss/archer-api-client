﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.Fields
{
    public enum FieldType
    {
        None = 0,
        Text = 1,
        Numeric = 2,
        Date = 3,
        ValuesList = 4,
        TrackingID = 6,
        ExternalLinks = 7,
        UsersGroupsList = 8,
        CrossReference = 9,
        Attachment = 11,
        Image = 12,
        CrossModuleStatusTracking = 14,
        RecordPermissions = 15,
        Matrix = 16,
        IpAddress = 19,
        RecordStatus = 20,
        FirstPublishedDate = 21,
        LastUpdatedDate = 22,
        RelatedRecords = 23,
        SubForm = 24,
        HistoryLog = 25,
        Discussion = 26,
        MultiReference = 27,
        QuestionnaireReference = 28,
        ContentAccessHistory = 29,
        Voting = 30,
        Scheduler = 31,
        CastScoreCard = 1001
    }
}
