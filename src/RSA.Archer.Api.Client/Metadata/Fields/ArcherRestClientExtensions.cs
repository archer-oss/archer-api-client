﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Metadata.Fields
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<List<FieldDefinitionResponse>> GetFieldsByLevelAsync(this ArcherRestClient source, int levelId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/fielddefinition/level/{levelId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<FieldDefinitionResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<List<FieldDefinitionResponse>> GetFieldsByApplicationAsync(this ArcherRestClient source, int applicationId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/fielddefinition/application/{applicationId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<FieldDefinitionResponse>[]>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.Select(x => x.RequestedObject).ToList();
        }

        public static async Task<FieldDefinitionResponse> GetFieldAsync(this ArcherRestClient source, int fieldId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/fielddefinition/{fieldId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<FieldDefinitionResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }
    }
}
