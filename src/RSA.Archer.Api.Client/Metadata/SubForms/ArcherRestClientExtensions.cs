﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Metadata.SubForms
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<SubFormResponse> GetSubFormAsync(this ArcherRestClient source, int subFormId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/subform/{subFormId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<SubFormResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }
    }
}
