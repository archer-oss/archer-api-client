﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using RSA.Archer.Api.Client.Metadata.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.SubForms
{
    public class SubFormResponse : ModuleResponse
    {
        public int LanguageId { get; set; }
    }
}
