﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.Metadata.ValuesLists
{
    public enum ValuesListDefinitionType
    {
        Custom = 0,
        Global = 1,
        Application = 2
    }
}
