﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Metadata.ValuesLists
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<ValuesListDefinitionResponse> GetValuesListAsync(this ArcherRestClient source, int valuesListId, SessionContext sessionContext)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var request = source.BuildRequest($"core/system/valueslist/{valuesListId}").WithSession(sessionContext);
            var response = await source.GetAsync<ArcherResponse<ValuesListDefinitionResponse>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }
    }
}
