﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.Metadata.ValuesLists
{
    public enum ValuesListDefinitionSortType
    {
        Custom = 0,
        Ascending = 1,
        Descending = 2,
        Random = 3
    }
}
