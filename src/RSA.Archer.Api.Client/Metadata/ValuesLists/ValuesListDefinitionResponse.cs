﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.ValuesLists
{
    public class ValuesListDefinitionResponse
    {
        public int? Id { get; set; }
        public Guid? Guid { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public virtual ValuesListDefinitionType Type { get; set; }
        public string Description { get; set; }
        public ValuesListDefinitionSortType SortType { get; set; }
        public bool IsSystem { get; set; }
        public int? LanguageId { get; set; }
        public DateTime LastUpdated { get; set; }
        public int? TaskId { get; set; }
    }
}
