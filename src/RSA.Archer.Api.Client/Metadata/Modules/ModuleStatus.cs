﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.Metadata.Modules
{
    public enum ModuleStatus
    {
        ActiveInDevelopment = 0,
        ActiveInProduction = 1,
        Archived = 2,
        Retired = 3
    }
}
