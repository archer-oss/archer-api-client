﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client.Metadata.Modules
{
    public enum ModuleType
    {
        BackOfficeCore = 1,
        Application = 2,
        Content = 3,
        InternalAdministration = 4,
        SubForm = 5,
        FrontOfficeCore = 6,
        Questionnaire = 7
    }
}
