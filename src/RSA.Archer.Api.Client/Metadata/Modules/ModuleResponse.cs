﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client.Metadata.Modules
{
    public abstract class ModuleResponse
    {
        public int? Id { get; set; }
        public Guid? Guid { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public ModuleType Type { get; set; }
        public ModuleStatus Status { get; set; }
        public string Description { get; set; }
        public string Directory { get; set; }
        public bool? IsDeleted { get; set; }
        public bool IsGeneric { get; set; }
        public bool IsSpellCheckEnabled { get; set; }
        public bool IsSystem { get; set; }
    }
}
