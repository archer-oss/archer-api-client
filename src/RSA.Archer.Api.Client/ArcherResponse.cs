﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System.Linq;
using System.Collections.Generic;
using System;
using System.Net.Http;

namespace RSA.Archer.Api.Client
{
    public class ArcherResponse<T> : ArcherResponse
    {
        public T RequestedObject { get; set; }
    }

    public class ArcherResponse
    {
        public bool IsSuccessful { get; set; }
        public IList<ValidationMessage> ValidationMessages { get; set; }
        public IList<Link> Links { get; set; }
    }
}
