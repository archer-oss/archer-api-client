﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client.Authentication
{
    public static class ArcherRestClientExtensions
    {
        public static async Task<SessionContext> LoginAsync(this ArcherRestClient source, string username, string password, string instanceName, string userDomain = null)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            LoginRequest req = new LoginRequest { Username = username, Password = password, InstanceName = instanceName, UserDomain = userDomain };
            var request = source.BuildRequest("core/security/login").WithValue(req);
            var response = await source.PostAsync<LoginRequest, ArcherResponse<SessionContext>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }

        public static async Task<bool> LogoutAsync(this ArcherRestClient source, SessionContext session)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (session == null) throw new ArgumentNullException(nameof(session));
            var request = source.BuildRequest("core/security/logout").WithSession(session).WithValue(new StringContainer { Value = session.SessionToken });
            var response = await source.PostAsync<StringContainer, ArcherResponse<bool>>(request).ConfigureAwait(false);
            response.EnsureSuccessful();
            return response.RequestedObject;
        }
    }
}
