﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Text;

namespace RSA.Archer.Api.Client
{
    public static class ArcherRequestExtensions
    {
        public static T AddHeader<T>(this T source, string name, string value) where T : ArcherRequest
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (name == null) throw new ArgumentNullException(nameof(name));
            source.Headers.Add(name, value);
            return source;
        }

        public static T WithHeaders<T>(this T source, IDictionary<string, string> headers) where T : ArcherRequest
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (headers == null) return source;

            foreach (var header in headers)
                source.Headers.Add(header.Key, header.Value);

            return source;
        }

        public static T WithSession<T>(this T source, SessionContext sessionContext) where T : ArcherRequest
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            source.SessionContext = sessionContext;
            return source;
        }

        public static ArcherRequest<T> WithValue<T>(this ArcherRequest source, T value)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            return new ArcherRequest<T>(source.ApiPath)
            {
                RequestPreprocessor = source.RequestPreprocessor,
                ResponsePreprocessor = source.ResponsePreprocessor,
                SessionContext = source.SessionContext,
                Value = value
            }.WithHeaders(source.Headers);
        }
    }
}
