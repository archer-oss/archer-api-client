﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RSA.Archer.Api.Client.HttpClientFactory;

namespace RSA.Archer.Api.Client
{
    public class ArcherRestClient
    {
        private readonly IHttpClientFactory httpClientFactory;

        #region Constructors

        public ArcherRestClient(string baseUri, IHttpClientFactory httpClientFactory = null)
          : this(new Uri(baseUri ?? throw new ArgumentNullException(nameof(baseUri))), httpClientFactory)
        {
        }

        public ArcherRestClient(Uri baseUri, IHttpClientFactory httpClientFactory = null)
        {
            if (baseUri == null) throw new ArgumentNullException(nameof(baseUri));
            if (!baseUri.AbsoluteUri.EndsWith("/", StringComparison.Ordinal))
                baseUri = new Uri(string.Concat(baseUri.AbsoluteUri, "/"));

            BaseUri = baseUri;
            this.httpClientFactory = httpClientFactory ?? new SingletonHttpClientFactory();
        }

        #endregion

        public Uri BaseUri { get; }

        public Uri BuildRequestUri(string apiPath)
        {
            apiPath = apiPath?.TrimStart('/', '\\'); //prevents the Uri combine from rooting the path to the domain because of beginning slash
            return new Uri(BaseUri, apiPath);
        }

        #region Get

        public Task<TResponse> GetAsync<TResponse>(ArcherRequest request)
        {
            return SendAsync<TResponse>(HttpMethod.Get, request);
        }

        #endregion

        #region Post

        public Task<TResponse> PostAsync<TRequest, TResponse>(ArcherRequest<TRequest> request)
        {
            return SendAsync<TRequest, TResponse>(HttpMethod.Post, request);
        }

        public Task<TResponse> PostAsync<TRequest, TResponse>(ArcherRequest request, TRequest value)
        {
            return SendAsync<TRequest, TResponse>(HttpMethod.Post, request, value);
        }

        #endregion

        #region Put

        public Task<TResponse> PutAsync<TRequest, TResponse>(ArcherRequest<TRequest> request)
        {
            return SendAsync<TRequest, TResponse>(HttpMethod.Put, request);
        }

        public Task<TResponse> PutAsync<TRequest, TResponse>(ArcherRequest request, TRequest value)
        {
            return SendAsync<TRequest, TResponse>(HttpMethod.Put, request, value);
        }

        #endregion

        #region Delete

        public Task<TResponse> DeleteAsync<TResponse>(ArcherRequest request)
        {
            return SendAsync<TResponse>(HttpMethod.Delete, request);
        }

        #endregion

        #region SendAsync

        public Task<TResponse> SendAsync<TResponse>(HttpMethod method, ArcherRequest request)
        {
            return SendAsync<TResponse>(method, request, null);
        }

        public Task<TResponse> SendAsync<TRequest, TResponse>(HttpMethod method, ArcherRequest<TRequest> request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            return SendAsync<TRequest, TResponse>(method, request, request.Value);
        }

        public Task<TResponse> SendAsync<TRequest, TResponse>(HttpMethod method, ArcherRequest request, TRequest value)
        {
            var content = GetJsonStringContent(value);
            return SendAsync<TResponse>(method, request, content);
        }

        public async Task<TResponse> SendAsync<TResponse>(HttpMethod method, ArcherRequest request, HttpContent content)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            //Do not dispose HttpClient ... https://stackoverflow.com/questions/15705092/do-httpclient-and-httpclienthandler-have-to-be-disposed
            //Do not dispose HttpRequestMessage or HttpResponseMessage objects ... https://github.com/aspnet/Security/issues/886
            HttpClient http = GetHttpClient();
            HttpRequestMessage msg = await BuildHttpRequestMessage(method, request, content).ConfigureAwait(false);
            HttpResponseMessage response = await http.SendAsync(msg).ConfigureAwait(false);

            if (request.ResponsePreprocessor != null)
                await request.ResponsePreprocessor(response).ConfigureAwait(false);

            return await ProcessResponse<TResponse>(response).ConfigureAwait(false);
        }

        #endregion

        #region Private Methods

        private static async Task<TResponse> ProcessResponse<TResponse>(HttpResponseMessage response)
        {
            response.EnsureSuccessStatusCode();

            string responseContentString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            if (typeof(TResponse) == typeof(string))
                return (TResponse)(object)responseContentString;

            var result = JsonConvert.DeserializeObject<TResponse>(responseContentString);
            return result;
        }

        private HttpClient GetHttpClient()
        {
            var http = httpClientFactory.GetHttpClient();
            return http;
        }

        private async Task<HttpRequestMessage> BuildHttpRequestMessage(HttpMethod method, ArcherRequest request, HttpContent content = null)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));

            var httpRequest = new HttpRequestMessage(method, BuildRequestUri(request.ApiPath));

            if (request.SessionContext != null)
                httpRequest.Headers.Authorization = new ArcherSessionAuthenticationHeaderValue(request.SessionContext.SessionToken);

            request.Headers?.ToList().ForEach(x => httpRequest.Headers.Add(x.Key, x.Value));
            httpRequest.Content = content;

            if (request.RequestPreprocessor != null)
                await request.RequestPreprocessor(httpRequest).ConfigureAwait(false);

            return httpRequest;
        }

        private static StringContent GetJsonStringContent<TRequest>(TRequest value)
        {
            string json = JsonConvert.SerializeObject(value);
            var content = new StringContent(json, Encoding.UTF8, MediaTypeNamesEx.Application.Json);
            return content;
        }

        #endregion
    }
}
