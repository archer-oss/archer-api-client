﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace RSA.Archer.Api.Client
{
    public static class ArcherResponseExtensions
    {
        /// <summary>
        /// Throws an exception if the response IsSuccessful property is false. The exception will contain any validation messages from the response.
        /// </summary>
        /// <param name="source">The response object.</param>
        public static void EnsureSuccessful(this ArcherResponse source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            if (source.IsSuccessful) return;

            var exceptions = source.ValidationMessages?.Select(x => new HttpRequestException(x.ResourcedMessage ?? x.Description)).ToList();
            if (exceptions?.Count == 1)
                throw exceptions[0];

            if (exceptions?.Count > 1)
                throw new AggregateException(exceptions);

            throw new HttpRequestException(Properties.Resources.RequestNotSuccessfulUnknownReasons);
        }

        /// <summary>
        /// Throws an exception if any response IsSuccessful property is false. The exception will contain any validation messages from the response.
        /// </summary>
        /// <param name="source">The collection of response objects.</param>
        public static void EnsureSuccessful(this IEnumerable<ArcherResponse> source)
        {
            if (source == null) throw new ArgumentNullException(nameof(source));
            var unsuccessful = source.Where(x => !x.IsSuccessful).ToList();
            if (unsuccessful.Count == 0) return;

            var exceptions = unsuccessful.Select(s => s.ValidationMessages?.Select(x => new HttpRequestException(x.ResourcedMessage ?? x.Description))).Where(x => x != null && x.Any()).SelectMany(x => x).ToList();
            if (exceptions?.Count == 1)
                throw exceptions[0];

            if (exceptions?.Count > 1)
                throw new AggregateException(exceptions);

            throw new HttpRequestException(Properties.Resources.RequestNotSuccessfulUnknownReasons);
        }
    }
}
