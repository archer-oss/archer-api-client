﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace System.Net.Mime
{
    /// <summary>
    /// This class is modeled after the .NET class of the same namespace called MediaTypeNames.
    /// This class simply adds additional media types that are missing from the .NET class.
    /// </summary>
    public static class MediaTypeNamesEx
    {
        public static class Application
        {
            public const string Json = "application/json";
        }
    }
}
