﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Text;

namespace RSA.Archer.Api.Client
{
    public static class ArcherRestClientExtensions
    {
        public static ArcherRequest BuildRequest(this ArcherRestClient source, string apiPath)
        {
            return new ArcherRequest(apiPath);
        }
    }
}
