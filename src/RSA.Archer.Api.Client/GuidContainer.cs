﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;

namespace RSA.Archer.Api.Client
{
    public class GuidContainer
    {
        public GuidContainer()
        {
        }

        public GuidContainer(Guid value)
        {
            Value = value;
        }

        public Guid Value { get; set; }
    }
}
