﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
namespace RSA.Archer.Api.Client
{
    public class StringContainer
    {
        public StringContainer()
        {
        }

        public StringContainer(string value)
        {
            Value = value;
        }

        public string Value { get; set; }
    }
}