﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/

// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Metadata.Levels.LevelResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Metadata.Modules.ModuleResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Metadata.ValuesLists.ValuesListDefinitionResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.DataFeed.DataFeedResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Metadata.Fields.FieldDefinitionResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1720:Identifier contains type name", Justification = "Matches JSON response", Scope = "member", Target = "~P:RSA.Archer.Api.Client.AccessControl.Groups.GroupResponse.Guid")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.DataFeed.DataFeedRunDetailResponse.DataFeedMessages")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Content.FieldContentRequest.ContentIds")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Content.FieldContentRequest.FieldIds")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Content.FieldContents.SubFormContent.ContentIds")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Content.FieldContents.CrossReferenceContent.ReferencedContents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.Content.ContentRecord.FieldContents")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.ArcherResponse.ValidationMessages")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "Set via JSON deserializer", Scope = "member", Target = "~P:RSA.Archer.Api.Client.ArcherResponse.Links")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "Naming matches header value closely", Scope = "member", Target = "~F:RSA.Archer.Api.Client.Headers.X_HTTP_METHOD_OVERRIDE")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1034:Nested types should not be visible", Justification = "Matching MIME type", Scope = "type", Target = "~T:System.Net.Mime.MediaTypeNamesEx.Application")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "HttpMessage objects do not need disposed per MS (https://github.com/aspnet/Security/issues/886)", Scope = "member", Target = "~M:RSA.Archer.Api.Client.ArcherRestClient.SendAsync``1(System.Net.Http.HttpMethod,RSA.Archer.Api.Client.ArcherRequest,System.Net.Http.HttpContent)~System.Threading.Tasks.Task{``0}")]
