﻿/*---------------------------------------------------------------------------------------------
 *  Copyright (c) 2019 RSA Security / RSA Archer
 *  Licensed under the MIT License. See LICENSE in the project root for license information.
 *--------------------------------------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace RSA.Archer.Api.Client
{
    public class ArcherRequest
    {
        public ArcherRequest(string apiPath)
        {
            this.ApiPath = apiPath;
        }

        public string ApiPath { get; }
        public IDictionary<string, string> Headers { get; } = new Dictionary<string, string>();
        public Func<HttpRequestMessage, Task> RequestPreprocessor { get; set; }
        public Func<HttpResponseMessage, Task> ResponsePreprocessor { get; set; }
        public SessionContext SessionContext { get; set; }
    }

    public class ArcherRequest<T> : ArcherRequest
    {
        public ArcherRequest(string apiPath) : base(apiPath)
        {
        }

        public ArcherRequest(string apiPath, T value) : base(apiPath)
        {
            Value = value;
        }

        public T Value { get; set; }
    }
}
